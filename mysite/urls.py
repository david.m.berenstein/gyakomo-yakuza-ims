from django.contrib import admin
from django.urls import include, path

from . import views


urlpatterns = [
    path('', views.index, name="Gyakomo Yakuza's yak shop"),
    path('admin/', admin.site.urls, name='admin'),
    path('admin/general_overview/', views.redirect_general_overview, name='general_overview'),
    path('admin/yak_upload/', views.upload_yak_xml, name='upload_yak_xml'),
    path('admin/order_upload/', views.upload_order_xml, name='upload_order_xml'),
    path('yak-shop/', include('myapi.urls'), name='api'),
]
