
import datetime

import requests
from django.http import HttpResponse, request
from django.http.response import HttpResponseRedirect
from django.shortcuts import redirect, render
from functional import add_order_xml_to_model, add_yak_xml_to_model
from rest_framework import response

from mysite import settings

from .forms import OrderForm


def index(request: request) -> redirect:
    """ redirect the main empty url path to the yak-shop landing page after validating the order form """
    if request.method == 'POST':
        form = OrderForm(request.POST)

        if form.is_valid():
            form = OrderForm()
            response = """ <h1 class="text-white mb-5">Thank You For Ordering!</h1> """
            time = request.POST['time']
            domain = settings.DEFAULT_DOMAIN
            order_url = f'{domain}yak-shop/order_fake/{time}/?format=json'
            order_data = {
                'customer': request.POST['customer'],
                'order': {
                    'milk': request.POST['milk'],
                    'skins': request.POST['skins']
                }
            }
            requests.post(order_url, json=order_data)

            return render(request, 'index_order.html', {'order_reply': response, 'form': form})
        else:
            return render(request, 'index_order.html', {'form': form})
    else:
        form = OrderForm()

    return render(request, 'index.html', {'form': form})


def redirect_general_overview(request: request):
    """ process admin request to get a API general overview of the yak stock """
    time_in_days = request.POST['time']

    return redirect(f'/yak-shop/overview/{time_in_days}/')


def upload_yak_xml(request: request):
    """ loads and forwards uploaded yak xml file content """
    uploaded_file = request.FILES['file']
    file_name = str(uploaded_file)
    file_content = uploaded_file.read().decode()

    if file_name != "":
        file_ext = file_name.split('.')[-1]
        if file_ext == 'xml':
            add_yak_xml_to_model(file_content)

    return redirect('/admin/myapi/yak/')


def upload_order_xml(request: request):
    """ loads and forwards uploaded order xml file content """
    uploaded_file = request.FILES['file']
    file_name = str(uploaded_file)
    file_content = uploaded_file.read().decode()

    if file_name != "":
        file_ext = file_name.split('.')[-1]
        if file_ext == 'xml':
            add_order_xml_to_model(file_content)

    return redirect('/admin/order/')
