import datetime
from typing import Any, Dict

from django import forms
from myapi.validators import validate_order, validate_postive_number


class OrderForm(forms.Form):
    customer = forms.CharField(label='Customer Name')
    created_at = datetime.datetime.now()
    time = forms.IntegerField(label='Time', validators=[validate_postive_number])
    milk = forms.DecimalField(label='Milk', validators=[validate_postive_number])
    skins = forms.IntegerField(label='Skins', validators=[validate_postive_number])

    def __init__(self, *args, **kwargs):
        super(OrderForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control flex-fill mr-0 mr-sm-2 mb-3 mb-sm-0'

    def clean(self) -> Dict[str, Any]:
        validate_order(entry=self.__dict__['cleaned_data'])



