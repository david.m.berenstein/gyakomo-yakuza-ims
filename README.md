# Gyakomo Yakuza IMS

A custom made inventory management (IMS) system for Gyakomo Yakuza the Yak evangelist.

# update Heroku config

$ git push heroku master


# run the server

$ python manage.py runserver

# merge new databasechanges

$ python manage.py makemigrations
$ python manage.py migrate

# set a new superuser

$ python manage.py createsuperuser

- username: gyakomo
- email: david.m.berenstein@gmail.com
- pwd: yakuza42


{
    "customer" : "gyakomo",
    "order" : { "milk" : 1100, "skins" : 3 }
}
