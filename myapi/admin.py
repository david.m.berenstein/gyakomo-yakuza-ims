from django.contrib import admin

from .models import Order, OrderAdmin, Yak, YakAdmin


admin.site.register(Yak, YakAdmin)
admin.site.register(Order, OrderAdmin)
