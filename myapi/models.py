from django.contrib import admin
from django.db import models

from myapi.validators import validate_order, validate_postive_number, validate_yak_age, validate_yak_sex


class YakAdmin(admin.ModelAdmin):
    list_display = ('name', 'age', 'sex')


class Yak(models.Model):
    name = models.CharField(max_length=60)
    age = models.FloatField(max_length=60, validators=[validate_yak_age])
    sex = models.CharField(max_length=60, validators=[validate_yak_sex])

    def __str__(self):
        return f'{self.name}, {self.age}, {self.sex}'


class Stock(models.Model):
    milk = models.FloatField(max_length=60)
    skins = models.IntegerField()

    def __str__(self):
        return f'{self.milk}, {self.skins}'


class Herd(models.Model):
    herd = models.CharField(max_length=1000)

    def __str__(self):
        return f'{self.herd}'


class OrderAdmin(admin.ModelAdmin):
    list_display = ('customer', 'created_at', 'time', 'milk', 'skins')

class Order(models.Model):
    customer = models.CharField(max_length=60)
    created_at = models.DateTimeField()
    time = models.IntegerField(validators=[validate_postive_number])
    milk = models.FloatField(max_length=60, validators=[validate_postive_number])
    skins = models.IntegerField(validators=[validate_postive_number])

    def clean(self) -> None:
        validate_order(entry=self.__dict__)

    def __str__(self):
        return f'{self.customer}, {self.created_at}, {self.time}, {self.milk}, {self.skins}'
