# Generated by Django 3.1.4 on 2020-12-25 13:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('myapi', '0002_yak'),
    ]

    operations = [
        migrations.CreateModel(
            name='Stock',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('milk', models.FloatField(max_length=60)),
                ('skins', models.IntegerField()),
            ],
        ),
    ]
