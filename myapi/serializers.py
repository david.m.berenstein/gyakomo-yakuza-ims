from rest_framework import serializers

from .models import Herd, Order, Stock, Yak


class YakSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Yak
        fields = ('name', 'age', 'sex')


class StockSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Stock
        fields = ('milk', 'skins')


class HerdSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Herd
        fields = ('herd', ('name', 'age', 'age-last-shaved'))


class OrderSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Order
        fields = ('customer', 'created_at', 'time', 'milk', 'skins')


