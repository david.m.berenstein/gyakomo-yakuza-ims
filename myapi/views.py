import json

from django.http import request
from django.http.response import JsonResponse
from functional import (check_validity_order, check_validity_username, get_herd_at_time_t, get_stock_at_time_t,
                        procces_order_request)
from rest_framework import status, viewsets
from rest_framework.decorators import api_view, renderer_classes
from rest_framework.renderers import BrowsableAPIRenderer, HTMLFormRenderer, JSONRenderer, StaticHTMLRenderer
from rest_framework.response import Response

from .models import Order, Yak
from .serializers import OrderSerializer, YakSerializer


class YakViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Yak.objects.all().order_by('name')
    serializer_class = YakSerializer

class OrderViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Order.objects.all()
    serializer_class = OrderSerializer


@api_view(('GET',))
@renderer_classes((BrowsableAPIRenderer, StaticHTMLRenderer))
def get_general_overview(_: request, time_in_days: int = 0) -> Response:
    stock_dict = get_stock_at_time_t(time_in_days=int(time_in_days))
    herd_dict = get_herd_at_time_t(time_in_days=int(time_in_days))

    overview = f"""In Stock:\n  {stock_dict['milk']} liters of milk\n  {stock_dict['skins']} skins of wool\nHerd:\n"""
    for yak in herd_dict['herd']:
        overview += f"  {yak['name']} {yak['age']} years old\n"

    return Response(overview)

@api_view(('GET',))
@renderer_classes((BrowsableAPIRenderer, JSONRenderer))
def get_current_stock(_: request, time_in_days: int=0) -> Response:
    stock_dict = get_stock_at_time_t(time_in_days=int(time_in_days))

    return Response(stock_dict)


@api_view(('GET',))
@renderer_classes((BrowsableAPIRenderer, JSONRenderer))
def get_current_herd(_: request, time_in_days:int=0) -> Response:
    herd_dict = get_herd_at_time_t(time_in_days=int(time_in_days))

    return Response(herd_dict)

@api_view(('POST', ))
@renderer_classes((BrowsableAPIRenderer, JSONRenderer))
def place_order(request: request, time_in_days: int = 0) -> JsonResponse:
    username_is_valid = check_validity_username(request=request)
    order_is_valid = check_validity_order(request=request)
    if username_is_valid and order_is_valid:
        response = procces_order_request(order_data=request.data, time_in_days=time_in_days)
    elif order_is_valid and not username_is_valid:
        problem = 'no valid username'
        solution = 'try to log in or ask Gyakomo to set you up with credentials'
        response = Response({'problem': problem, 'solution': solution}, status=status.HTTP_401_UNAUTHORIZED)
    elif username_is_valid and not order_is_valid:
        problem = 'no valid order information'
        solution = {'order format': {'customer': 'name', 'order':{'milk': 0, "skins": 0}}}
        response = Response({'problem': problem, 'solution': solution}, status=status.HTTP_400_BAD_REQUEST)
    else:
        problem = 'no valid order information and usernamme'
        solution = 'ask Gyakomo to set you up with some general introduction'
        response = Response({'problem': problem, 'solution': solution}, status=status.HTTP_400_BAD_REQUEST)

    return response


@api_view(('POST', ))
@renderer_classes((BrowsableAPIRenderer, JSONRenderer))
def fake_place_order(request: request, time_in_days: int = 0) -> JsonResponse:
    username_is_valid = check_validity_username(request=request)
    order_is_valid = check_validity_order(request=request)
    if username_is_valid and order_is_valid:
        response = procces_order_request(order_data=request.data, time_in_days=time_in_days, fake=True)
    elif order_is_valid and not username_is_valid:
        problem = 'no valid username'
        solution = 'try to log in or ask Gyakomo to set you up with credentials'
        response = Response({'problem': problem, 'solution': solution}, status=status.HTTP_401_UNAUTHORIZED)
    elif username_is_valid and not order_is_valid:
        problem = 'no valid order information'
        solution = {'order format': {'customer': 'name', 'order': {'milk': 0, "skins": 0}}}
        response = Response({'problem': problem, 'solution': solution}, status=status.HTTP_400_BAD_REQUEST)
    else:
        problem = 'no valid order information and usernamme'
        solution = 'ask Gyakomo to set you up with some general introduction'
        response = Response({'problem': problem, 'solution': solution}, status=status.HTTP_400_BAD_REQUEST)

    return response

