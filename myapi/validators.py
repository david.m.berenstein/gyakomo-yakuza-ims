
# Create your tests here.
from os import error

import requests
from django.conf import settings
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError


def validate_postive_number(value):
    """ validates if an entry is a positive number """
    if value < 0:
        raise ValidationError('Your input should be a postive number.')

def validate_yak_age(value):
    """ validates if a entered tak age is valid i.e. between 0 and 10 """

    if value > 10 or value < 0:  # Your conditions here
        raise ValidationError('%s is not accepted as age. Yaks are born with an age of 0 and die at an age of 10.' % int(value))

def validate_yak_sex(value):
    """ validates if a entered yak sex is valid i.e. male or female """
    if value not in ['m', 'f']:
        raise ValidationError('%s is not accepted as sex. Try using "f" for female or "m" for male.' % value)


def validate_order(entry):
    """ validates if a entered order is valid using the available api """
    domain = settings.DEFAULT_DOMAIN
    time_in_days = int(entry['time'])
    order_url = f'{domain}yak-shop/order_fake/{time_in_days}/?format=json'
    order_data = {
        'customer': 'gyakomo',
        'order': {
            'milk': float(entry['milk']),
            'skins': int(entry['skins'])
        }
    }

    response = requests.post(order_url, json=order_data)
    content = response.json()
    status_code = response.status_code

    error_dict = {}
    if len(list(User.objects.all().filter(username=entry['customer']))) != 1:
        error_dict['customer'] = ('Customer is not present in the database. First register the new customer.')

    if status_code == 206:
        if content['milk'] == order_data['order']['milk']:
            error_dict['skins'] = ('There only are %s skins available.' % (content['skins']))
        elif content['skins'] == order_data['order']['skins']:
            error_dict['milk'] = ('There only are %s liters of milk available.' % (content['milk']))
        else:
            error_dict['skins'] = ('There only are %s skins available.' % (content['skins']))
            error_dict['milk'] = ('There only are %s liters of milk available.' % (content['milk']))

    if status_code == 404:
        error_dict['milk'] = ('There is no milk left at this moment in time.')
        error_dict['skins'] = ('There are no skins left at this moment in time.')

    if len(error_dict) > 0:
        raise ValidationError(error_dict)


