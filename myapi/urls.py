from django.urls import include, path
from django.views.generic import RedirectView
from rest_framework import routers
from rest_framework.decorators import api_view
from rest_framework.response import Response

from . import views


router = routers.DefaultRouter()
router.register(r'yaks', views.YakViewSet)
router.register(r'orders', views.OrderViewSet)


@api_view(['GET'])
def apiOverview(request):
    host_base_url = request.get_host()
    base_url = f'http://{host_base_url}/yak-shop/'
    api_urls = {
        'Base API': base_url,
        'GET': {
            'Yak List': base_url + 'yaks',
            'Yak Instance': base_url + 'yaks/1/',
            'Order List': base_url + 'orders',
            'Order Instance': base_url + 'orders/1/',
            'Stock Status': base_url + 'stock/1/',
            'Herd Status': base_url + 'herd/1/',
            'General Overview': base_url + 'overview/1/',
        },
        'POST': {
            'Place Order': base_url + 'order/1/'
        }
    }

    return Response(api_urls)

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    path('', apiOverview, name="api-overview"),
    path('', include(router.urls)),
    path('order/', RedirectView.as_view(url='1/')),
    path('order/<int:time_in_days>/', views.place_order),
    path('order_fake/<int:time_in_days>/', views.fake_place_order),
    path('overview/', RedirectView.as_view(url='1/')),
    path('overview/<int:time_in_days>/', views.get_general_overview),
    path('stock/', RedirectView.as_view(url='1/')),
    path('stock/<int:time_in_days>/', views.get_current_stock),
    path('herd/', RedirectView.as_view(url='1/')),
    path('herd/<int:time_in_days>/', views.get_current_herd),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
]



