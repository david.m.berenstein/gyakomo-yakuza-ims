from datetime import datetime
from typing import Tuple
from xml.dom import minidom

import pandas as pd
import requests
from django.contrib.auth.models import User
from django.db.models import Sum
from django.http import request
from rest_framework import status
from rest_framework.response import Response

from myapi.models import Order, Yak
from mysite import settings


def add_yak_xml_to_model(xml_content: str) -> None:
    """
    [summary] parses the xml-formatted string and adds model elements to the Yak database

    Args:
        xml_content (str): [description] xml-formatted string with yak info
    """
    xmldoc = minidom.parseString(xml_content)
    yak_entries = xmldoc.getElementsByTagName('labyak')
    for yak in yak_entries:
        name, age, sex = yak.attributes['name'].value, yak.attributes['age'].value, yak.attributes['sex'].value
        Yak(name=name, age=age, sex=sex).save()

    return None


def add_order_xml_to_model(xml_content: str) -> None:
    """
    [summary] parses the xml-formatted string and adds model elements to the Yak database

    Args:
        xml_content (str): [description] xml-formatted string with yak info
    """
    domain = settings.DEFAULT_DOMAIN
    xmldoc = minidom.parseString(xml_content)
    order_entries = xmldoc.getElementsByTagName('order')
    for order in order_entries:
        customer, time, milk, skins = (
            order.attributes['customer'].value,
            order.attributes['time'].value, order.attributes['milk'].value, order.attributes['skins'].value
        )
        order_url = f'{domain}yak-shop/order_fake/{time}/?format=json'
        order_data = {
            'customer': customer,
            'order': {
                'milk': milk,
                'skins': skins
            }
        }

        requests.post(order_url, json=order_data)

    return None


def get_stock_at_time_t(time_in_days: int) -> dict:
    """
    [summary] retrieves a stock level based on the transcended time in yak days

    Args:
        time_in_days (int): [description] time in yak days

    Returns:
        dict: [description] milk and skin stock at time ti
    """
    milk_stock = get_milk_at_time_t(time_in_days)
    skin_stock = get_skins_at_time_t(time_in_days)

    return {'milk': milk_stock, 'skins': skin_stock}


def get_milk_at_time_t(time_in_days: int) -> float:
    """
    [summary] retrieves a stock level for milk based on the transcended time in yak days
    Each day a LabYak produces 50-D*0.03 liters of milk
    A LabYak dies the day he turns 10.
    Only for female yaks

    Args:
        time_in_days (int): [description] time in yak days

    Returns:
        float: [description] a milk stock level
    """
    yak_herd = Yak.objects.all()
    milk_stock = 0.0

    for yak in yak_herd:
        if yak.sex == 'f':
            concurrent_days = list(range(time_in_days))
            age_at_concurrent_days = [yak.age+get_time_in_yak_years(day) for day in concurrent_days]
            milk_stock += sum([50-((get_time_in_yak_days(age))*0.03) for age in age_at_concurrent_days if age < 10])

    return milk_stock


def get_skins_at_time_t(time_in_days: int) -> int:
    """
    [summary] retrieves a stock level for milk based on the transcended time in yak days
    by executing the get_produced_skins_and_age_last_shaved() function for every yak

    Args:
        time_in_days (int): [description] time in yak days

    Returns:
        int: [description] a skin stock level
    """
    yak_herd = Yak.objects.all()
    skin_stock = 0

    for yak in yak_herd:
        produced_skins, _ = get_produced_skins_and_age_last_shaved(time_in_days, yak)
        skin_stock += produced_skins

    return skin_stock


def get_produced_skins_and_age_last_shaved(time_in_days: int, yak: Yak) -> Tuple:
    """
    [summary] retrieves the number of produced skins and last shaving date for a yak
    At most every 8+D*0.01 days you can again shave a LabYak
    A LabYak dies the day he turns 10.
    A yak’s first shave can occur at the age of 1 year.
    The moment you open the YakShop online will be day 0, and all yaks will be eligible to be shaved.

    Args:
        time_in_days (int): [description] time in yak days
        yak (Yak): [description] a yak model entry

    Returns:
        Tuple: [description] a tuple containing the number of produced skins and last shaving age
    """
    days_till_next_shave = 0
    produced_skins = 0
    concurrent_days = list(range(max(2,time_in_days)))
    age_at_concurrent_days = [yak.age+get_time_in_yak_years(day) for day in concurrent_days]
    shaving_age = max(1, age_at_concurrent_days[0])

    while shaving_age < max(age_at_concurrent_days):
        days_till_next_shave = (8 + get_time_in_yak_days(shaving_age)*0.01)
        shaving_age += get_time_in_yak_years(days_till_next_shave)
        produced_skins += 1


    age_last_shaved = None
    if (days_till_next_shave == 0) and (age_at_concurrent_days[0] >=1):
        age_last_shaved = age_at_concurrent_days[0]
    else:
        age_last_shaved = shaving_age - get_time_in_yak_years(days_till_next_shave)

    return produced_skins, age_last_shaved


def get_herd_at_time_t(time_in_days: int) -> dict:
    """
    [summary] retrieve up-to-date yak information for the herd at time t

    Args:
        time_in_days (int): [description] time in yak days

    Returns:
        dict: [description] a dictionary containing herd information
    """
    yak_herd = list(Yak.objects.all())
    herd_dict = {'herd': []}

    for yak in yak_herd:
        current_age = float(yak.age) + get_time_in_yak_years(time_in_days)
        _, age_last_shaved = get_produced_skins_and_age_last_shaved(time_in_days, yak)

        if current_age >= 10:
            current_age = 'passed_away'

        herd_dict['herd'].append(
            {'name': yak.name, 'age': current_age, 'age-last-shaved': age_last_shaved}
        )

    return herd_dict


def get_time_in_yak_years(time_in_days: int) -> float:
    """ transform time in yak days to time in yak years """
    return time_in_days/100


def get_time_in_yak_days(time_in_years: int) -> float:
    """ transform time in yak years to time in yak days """
    return time_in_years*100


def get_stock_requirements_from_time_t(time_in_days: int) -> pd.DataFrame:
    """
    [summary] determines the available stock throughout time based on the production and existing orders

    Args:
        time_in_days (int): [description] time in yak days

    Returns:
        pd.DataFrame: [description] a datafraje containing the availability
    """
    order_df = pd.DataFrame(list(Order.objects.values('time').annotate(milk=Sum('milk'), skins=Sum('skins')).values()))

    if not len(order_df):
        order_df = pd.DataFrame(columns=['time', 'milk', 'skins'])

    order_df.set_index('time', inplace=True)
    order_df = order_df[['milk', 'skins']]
    check_time_in_orderbook = len(list(Order.objects.all().filter(time=time_in_days))) != 0

    if not check_time_in_orderbook:
        order_df.loc[time_in_days] = [0, 0]

    order_df = order_df.sort_index(ascending=True)
    order_df['cumsum_req_milk'] = order_df['milk'].cumsum()
    order_df['cumsum_req_skins'] = order_df['skins'].cumsum()
    order_df['produced_milk'] = order_df.index.map(get_milk_at_time_t)
    order_df['produced_skins'] = order_df.index.map(get_skins_at_time_t)
    order_df['available_milk'] = order_df['produced_milk'] - order_df['cumsum_req_milk']
    order_df['available_skins'] = order_df['produced_skins'] - order_df['cumsum_req_skins']
    order_df = order_df[order_df.index >= time_in_days]

    return order_df

def procces_order_request(order_data: dict, time_in_days: int, fake: bool=False) -> Response:
    """
    [summary] processes the order based on available order data in the database.
    Also takes into account the potential influence of orders on earlier placed order later in time.
    The actual order data is based on the minimum availability of the order over time.

    Args:
        order_data (dict): [description] a dictionary containing information for the order
        time_in_days (int): [description] the time at which the order should be placed

    Returns:
        Response: [description] a response 404, 402, 401 for no stock, partial stock and full stock
    """
    order_df = get_stock_requirements_from_time_t(time_in_days=time_in_days)
    actual_order = {
        'milk': min(order_df['available_milk'].min(), order_data['order']['milk']),
        'skins': min(order_df['available_skins'].min(), order_data['order']['skins'])
    }

    check_no_actual_stock_available = (actual_order['milk'] == 0) and (actual_order['skins'] == 0)
    if check_no_actual_stock_available:
        response = Response({}, status=status.HTTP_404_NOT_FOUND)
    else:
        if not fake:
            Order(
                customer=order_data['customer'],
                created_at=datetime.now(),
                time=time_in_days,
                milk=actual_order['milk'],
                skins=actual_order['skins']
            ).save()

        if actual_order == order_data['order']:
            response = Response(actual_order, status=status.HTTP_201_CREATED)
        else:
            response = Response(actual_order, status=status.HTTP_206_PARTIAL_CONTENT)

    return response

def delete_most_recent_order():
    Order.objects.all()[-1].delete()


def check_validity_order(request: request) -> bool:
    """
    [summary] checks if the data provided with the request is in the correct format

    Args:
        request (request): [description] a request from the api

    Returns:
        [type]: [description] outcome of the test
    """
    order_is_valid = False
    data = request.data
    if 'order' in data and 'customer' in data:
        if 'milk' in data['order'] and 'skins' in data['order']:
            order_is_valid = True

    return order_is_valid


def check_validity_username(request: request) -> bool:
    """
    [summary] check if the username from the request align with the username in the database log-in.
    if a admin user

    Args:
        request (request): [description] a request from the api

    Returns:
        [bool]: [description] outcome of the test
    """
    if 'customer' in request.data:
        username = request.data['customer'].strip()
    else:
        username = ''

    check_username_combined = False
    check_username_request = True
    check_username_database = len(list(User.objects.all().filter(username=username))) == 1

    if check_username_database and check_username_request:
        check_username_combined = True

    return check_username_combined
